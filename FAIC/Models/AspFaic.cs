﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Faic.Models
{
    public class AspFaic : DbContext
    {
     
        public AspFaic(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> ProductsInfo { get; set; }

    }
}
